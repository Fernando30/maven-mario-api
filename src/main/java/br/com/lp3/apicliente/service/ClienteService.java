package br.com.lp3.apicliente.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lp3.apicliente.dto.ClienteDTO;
import br.com.lp3.apicliente.model.Cliente;
import br.com.lp3.apicliente.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;

	public ClienteDTO findById(Long id) {
		Optional<Cliente> oCliente = clienteRepository.findById(id);
		if (oCliente != null && oCliente.isPresent()) {
			Cliente cliente = oCliente.get();
			ClienteDTO clienteDTO = new ClienteDTO(cliente.getId(), cliente.getNome(), cliente.getCpf());
			return clienteDTO;
		} else {
			return null;
		}
		
	}
	
	public ClienteDTO saveCliente(ClienteDTO clienteDTO) {
		Cliente cliente = new Cliente(clienteDTO.getId(), clienteDTO.getNome(), clienteDTO.getCpf());
		cliente = clienteRepository.save(cliente);
		clienteDTO.setId(cliente.getId());
		return clienteDTO;
	}
	
}
