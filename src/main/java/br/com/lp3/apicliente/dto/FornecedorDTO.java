package br.com.lp3.apicliente.dto;

public class FornecedorDTO {
	public int id;
	
	public String nome;
	
	public String endereco;
	
	public long cnpj;
	
	public String razaoSocial;

	public FornecedorDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FornecedorDTO(int id, String nome, String endereco, long cnpj, String razaoSocial) {
		super();
		this.id = id;
		this.nome = nome;
		this.endereco = endereco;
		this.cnpj = cnpj;
		this.razaoSocial = razaoSocial;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public long getCnpj() {
		return cnpj;
	}

	public void setCnpj(long cnpj) {
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	
}
